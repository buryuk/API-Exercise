# EasyOpenAPI in K8
EasyOpenAPI in K8 is easy-to-use tool to deploy an API with database in orchestration with help from containers. 
# Tools
1. Python 3 with Flask and Swagger UI
2. MongoDB 4.4 as database
3. docker-compose for creating docker images
4. Kubernetes for container-orchestration 
# Directories
- /api: API application and resources files
- /db: Database deployment and image creation files
- /k8: Kubernetes deployment files
# Installation
1. In the root of this project folder, execute below: 
```sh
$ ./buildanddeploy.sh
```
It created all necessary Docker images and Kubernetes resources. 
2. Go to k8 folder and execute below: 
```sh
$ delete.sh
```
It deletes all used Docker images and Kubernetes resources. 
# API Usage
You can go to http://api-app/swagger_ui address from inside of your Kubernetes cluster for API examples and documentation. 

# Steps 
### Docker Compose
#### Setup & fill database
Docker creates all necessary images for app and database. 
**Created Images**
app: Application execution
mongo-db: MongoDB 
mongo-seed: Using for data injection to MongoDB
**Kubernetes Deployment**
1. api-app deployment is created with api-app node and api-app service. 
2. mongo-pv persistent volume created. 
3. mongo-db service and mongo-claim persistent volume claim are created with statefulsets. 
4. mongo-seed job is created for filling the MongoDB database with data via mongoimport command from db/titanic.csv. A Busybox InitContainer checks mongo-db service to start then mongoimport command is executed. After 100 TTL (time to live) seconds, all Complete and Failed jobs are deleted. 
5. Kubernetes Secret is implemented and used in MongoDB username and password. 
6. Jenkins is implemented with Postman-Newman tool for run API tests. 

#Jenkins Implementation
After deployment finish, Jenkins is reachable with http://jenkins:8080 URL. 
Go to http://jenkins:8080 URL. 
It will request one time admin password. You can find it with below code:
```sh
$ kubectl logs service/jenkins
```

It will be written in the log like below:
```sh
*************************************************************
*************************************************************
*************************************************************

Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:

6c1da8caa72c454ab94634ef7356318c

This may also be found at: /var/jenkins_home/secrets/initialAdminPassword

*************************************************************
*************************************************************
*************************************************************
```

It will request to create first admin user and information. Fill it with your details. 
In Jenkins follow below steps: 
*go to Manage Kenkins> Manage Plugins > Available Tab > search "nodejs" > Find NodeJS Plugin and click Install checkbox and then click Install without restart button. 
This will add NodeJS Plugin to your Jenkins.
Then, go to Manage Kenkins > Global Tool Configuration > NodeJS section > Add NodeJS and enter Name for your NodeJS installation, for example "NodeJS 16.5.0" > write "newman" as Global npm packages install > click Save. 
Thi will add a new NodeJS with newman to your Jenkins. 
Go to Jenkins Dashboard > New Item > FreeStyle project with a name of your choice, click Ok > select "Provide Node & npm bin/ folder to PATH" > click Add build step as Execute shell and write below as Command: 
```sh
newman run https://www.getpostman.com/collections/1884eda10679b3473ff4
```
This will run a predefined API test script. 
You can click Build Now to run your tests in your newly created project. 


# NOTES
1. In Minikube environment, please do not forget to use below command for setup enviornment variables for Minikube's Docker environment:
```sh
$ eval $(minikube -p minikube docker-env)
```
2. Because of mongoimport add records to the dataabase with ObjectID(oid), _id section will look like below:
```sh
"_id": {"$oid": "61013e48a909f518cb808e37"
```
Please see expected output below: 

| HTTP Verb | Path             | Request Content-Type | Request body | Response Content-Type | Example response body |
|-----------|------------------|----------------------|--------------|-----------------------|-----------------------|
| GET       | `/people`        | `application/json`   | -            | `application/json`    | `[{"_id": {"$oid": "61008b4abd6a1e66f851c9d8"}, "Survived": 1, "Pclass": 1, "Name": "Mrs. John Bradley (Florence Briggs Thayer) Cumings", "Sex": "female", "Age": 38, "Siblings/Spouses Aboard": 1, "Parents/Children Aboard": 0, "Fare": 71.2833}, {"_id": {"$oid": "61008b4abd6a1e66f851c9d9"}, "Survived": 1, "Pclass": 3, "Name": "Miss. Laina Heikkinen", "Sex": "female", "Age": 26, "Siblings/Spouses Aboard": 0, "Parents/Children Aboard": 0, "Fare": 7.925}, ...]` |
| POST      | `/people`        | `application/json`   | `{"_id": {"$oid": "61011f0da909f518cb808e1e"}, "survived": true, "passengerClass": 3, "name": "Mr. Owen Harris Braund", "sex": "male", "age": 22, "siblings0rSpouseAboard": 1, "parents0rChildrenAboard": 0, "fare": 7.25}` |
| GET       | `/people/{oid}` | `application/json`   | -            | `application/json`    | `{"_id": {"$oid": "61011f0da909f518cb808e1e"}, "survived": true, "passengerClass": 3, "name": "Mr. Owen Harris Braund", "sex": "male", "age": 22, "siblings0rSpouseAboard": 1, "parents0rChildrenAboard": 0, "fare": 7.25}` |
| DELETE    | `/people/{oid}` | `application/json`   | -            | `application/json`    | `{"Status Code": 200, "message": "User with id : 61011f0da909f518cb808e1e has been deleted successfully"}` |
| PUT       | `/people/{oid}` | `application/json`   | `{ "survived": true, "passengerClass": 3, "name": "Mr. Owen Harris Braund", "sex": "male", "age": 23, "siblingsOrSpousesAboard": 1, "parentsOrChildrenAboard":0, "fare":7.25}` | `application/json`    | `{"Status Code": 200, "message": "User with id : 61011f0da909f518cb808e1e has been updated successfully"}` |