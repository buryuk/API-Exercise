#!/usr/bin/env bash

# This script creates all necessary Docker images and Kubernetes resources

echo "---Docker images are creating..."
docker-compose build
cd k8/
echo "---Kubernetes Resources are creating"
kubectl create -f .