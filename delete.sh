#!/usr/bin/env bash

# This script delete all Kubernetes resource and Docker image from your system

echo "---Kubernetes Resources are deleting"
kubectl delete deploy api-app
kubectl delete deploy jenkins
kubectl delete service api-app
kubectl delete service jenkins
kubectl delete service mongo-db
kubectl delete statefulset mongo-db
kubectl delete pvc mongo-claim-mongo-db-0
kubectl delete pvc mongo-claim-mongo-db-1
kubectl delete pv mongo-pv
kubectl delete secret mysecret
kubectl delete job.batch/seed

echo "---Docker images are deleting"
docker rmi app:latest
docker rmi mongo-seed:latest
docker rmi mongodb:latest
docker rmi python:3
docker rmi mongo:4.4
docker rmi jenkins/jenkins:lts-jdk11
docker rmi alpine:latest
docker rmi busybox:1.28